﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZifLib
{
    public abstract class IndexMCBase<T, TI> : IIndex<T, TI>
    {
        protected IndexMCBase(IMCController<T> coll, string name, Func<T, TI> f)
        {
            Name = name;
            Coll = coll;
            F = f;
        }
        public IMCController<T> Coll { get; }
        public string Name { get; }
        public Func<T, TI> F { get; }

        protected T Ix(int a) => Coll[a];
        protected int RemoveRequest(IEnumerable<int> li_) => Coll.Locker.Write(() =>
         {
             if (li_ == null) return 0;
             var li = li_.ToList();
             li.Sort((a, b) => b - a);
             foreach (var i in li) Coll.RemoveAt(i);
             return li.Count;
         });

        protected abstract IReadOnlyCollection<int> GetZ(TI h);

        public IReadOnlyCollection<T> Get(TI x) => Coll.Locker.Read(() => GetZ(x)?.Select(Ix)?.ToArray() ?? new T[0]);
        public IReadOnlyCollection<T> GetOfObj(T x) => Coll.Locker.Read(() => GetZ(F(x))?.Select(Ix)?.ToArray() ?? new T[0]);

        public abstract int Remove(TI x);
        public abstract int RemoveOfObj(T x);
    }
}
