﻿using System;
using System.Collections.Generic;
using System.Linq;
using ZifLib.Utils.Collection;

namespace ZifLib
{
    internal class Index<T, TI> : IndexMCBase<T, TI>, IIndexController<T, TI>
    {
        private readonly IDictionary<TI, IList<int>> hasher;

        public override int Remove(TI x) => RemoveRequest(GetX(x));
        public override int RemoveOfObj(T x) => Remove(F(x));
        
        protected override IReadOnlyCollection<int> GetZ(TI h) => hasher.GetOrDefault(h, null)?.ToList();

        protected IList<int> GetX(TI h) => hasher.GetOrDefault(h, null);
        IList<int> GetXObj(T x) => GetX(F(x));
        protected IList<int> GetCreate(TI x) => hasher.Create(x, () => new List<int>());
        protected IList<int> GetCreateI(int x) => GetCreate(F(Coll[x]));

        public void AddIndex(int index) => GetCreateI(index).Add(index);

        void IIndexController<T>.RemoveReplaceIndex(int newind, int oldind)
        {
            var controller = ((IIndexController<T>)this);
            controller.RemoveIndex(newind);
            controller.ReplaceIndex(oldind, newind);
        }
        void IIndexController<T>.RemoveIndex(int ind)
        {
            var ObjTi = F(Ix(ind));
            var li = GetXObj(Ix(ind));
            if (li != null)
            {
                li.Remove(ind);

                if (li.Count == 0)
                    Rem(ObjTi);
            }
        }
        void IIndexController<T>.ReplaceIndex(int oldIndex, int newIndex)
        {
            if (oldIndex == newIndex) return;
            var il = GetXObj(Ix(oldIndex));
            if (il != null) il[il.IndexOf(oldIndex)] = newIndex;
        }


        public void Clear() => hasher.Clear();

        protected void Rem(TI ObjTi) => hasher.Remove(ObjTi);

        internal Index(IMCController<T> coll, string name, Func<T, TI> f, IEqualityComparer<TI> comparer = null)
            : base(coll, name, f)
        {
            hasher = new Dictionary<TI, IList<int>>(comparer ?? EqualityComparer<TI>.Default);
        }
    }
}
