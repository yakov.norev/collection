﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ZifLib.Utils.Collection;

namespace ZifLib
{
    internal class IndexU<T, TI> : IndexMCBase<T, TI>, IUIndexController<T, TI>
    {
        private readonly IDictionary<TI, int> hasher;


        protected int? Get(TI h) => hasher.GetNullable(h);
        protected int? GetXObj(T x) => Get(F(x));

        //ICollection<int> GetY(TI h) => GetX(h)?.CopyR() ?? new int[0];
        //ICollection<int> GetYObj(T x) => GetY(F(x));
        //protected  IList<int> GetCreate(TI x) => hasher.Create(x, () => new List<int>());		
        public void Clear() => hasher.Clear();

        internal IndexU(IMCController<T> coll, string name, Func<T, TI> f, IEqualityComparer<TI> comparer = null)
            : base(coll, name, f)
        {
            hasher = new Dictionary<TI, int>(comparer ?? EqualityComparer<TI>.Default);
        }

        protected bool RemoveRequest(int? i)
        {
            if (i == null) return false;
            Coll.RemoveAt(i.Value);
            return true;
        }
        public void AddIndex(int index) => Coll.Locker.Write(() => hasher[F(Coll[index])] = index);

        public void RemoveReplaceIndex(int newind, int oldind)
        {
            RemoveIndex(newind);
            ReplaceIndex(oldind, newind);
        }
        public void RemoveIndex(int ind)
        {
            Rem(F(Coll[ind]));
        }
        public void ReplaceIndex(int oldIndex, int newIndex)
        {
            if (oldIndex == newIndex) return;
            hasher[F(Coll[oldIndex])] = newIndex;
        }
        public bool Test(T a, int ind)
        {
            var v = hasher.GetNullable(F(a));
            return v.HasValue && v != ind;
        }
        public bool Test(T a)
        {
            return hasher.ContainsKey(F(a));            
        }

        protected override IReadOnlyCollection<int> GetZ(TI x)
        {
            var t = Index_n(x);
            if (t == null) return null;
            return new int[] { (int)t };
        }
        //   public IReadOnlyCollection<T> GetOfObj(T x) => Get(F(x));
        public int Index(TI x) => Index_n(x) ?? -1;//because threadsafe
        public int? Index_n(TI x) => Coll.Locker.Read(() => Get(x));//because threadsafe
        public T Get1(TI x) => Coll.Locker.Read(() =>
         {
             var v = Get(x);
             if (v == null) return default(T);
             return Coll[v.Value];
         });
        T IUIndexG<T>.GetOfObj1(T x) => Get1(F(x));

        public override int Remove(TI x) => Remove1(x) ? 1 : 0;
        public override int RemoveOfObj(T x) => Remove(F(x));
        public bool Remove1(TI x) => RemoveRequest(Get(x));
        public bool RemoveOfObj1(T x) => Remove1(F(x));
        protected void Rem(TI ObjTi) => hasher.Remove(ObjTi);
        public void Index(TI x, Action<int> f) => Coll.Locker.Read(() =>
         {
             var f1 = Get(x);
             if (f1.HasValue) f(f1.Value);
         });

        public bool GetCreate(TI x, Action<int> f, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = Get(x);
            if (f1.HasValue)
            {
                var t = f1.Value;
                f(t);
                return true;
            }
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !F(t1).Equals(x)) Debugger.Break();
                Coll.Add(t1);
                f1 = Get(x);
                if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return false;
            }
        });
        public T GetCreateR(TI x, Action<int> f, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = Get(x);
            if (f1.HasValue)
            {
                var t = f1.Value;
                f(t);
                return Coll[t];
            }
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !F(t1).Equals(x)) Debugger.Break();
                Coll.Add(t1);
                //    f1 = GetX(x);
                //  if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return t1;
            }
        });

        public T GetCreate(TI x, Func<T> g) => Coll.Locker.Read(() =>
        {
            var f1 = Get(x);
            if (f1.HasValue) return Coll[f1.Value];
            else
            {
                var t1 = g();
                if (Debugger.IsAttached && !x.Equals(F(t1))) Debugger.Break();
                Coll.Add(t1);
                // f1 = GetX(x);
                // if (Debugger.IsAttached && (!f1.HasValue || !Coll[f1.Value].Equals(t1))) Debugger.Break();
                return t1;
            }
        });

    
    }
}
