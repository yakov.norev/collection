﻿using System;
using System.Collections.Generic;

namespace ZifLib
{
    public interface IndexColl<T> : IEnumerable<IIndexG<T>>
    {
        IIndexG<T> this[string key] { get; }
        IUIndex<T, T1> CreateU<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer = null, string name = null);
        IIndex<T, T1> Create<T1>(Func<T, T1> col, bool unique = false, IEqualityComparer<T1> comparer = null, string name = null);
        IIndex<T, T1> Create<T1>(IIndexController<T, T1> ind);
        IRelationTyped<T, T1> CreateRel<T1>(Func<T, IDictionary<T1, object>> col);
    }
    public interface IMCController<T> : IRWLockable, IList<T>
    {
    }

    public interface IIndex : IRelation
    {
        //	bool Unique { get; }
    }
    public interface IIndexG<T> : IIndex
    {
        IReadOnlyCollection<T> GetOfObj(T x);
        int RemoveOfObj(T x);
    }
    public interface IIndex<T, T1> : IIndexG<T>
    {
        IReadOnlyCollection<T> Get(T1 x);
        int Remove(T1 x);
    }

    public interface IIndexController<T> : IIndexG<T>
    {
        IMCController<T> Coll { get; }
        void AddIndex(int c);
        void Clear();
        void RemoveReplaceIndex(int newind, int oldind);
        void ReplaceIndex(int oldind, int newind);
        void RemoveIndex(int ind);
    }
    public interface IIndexController<T, TI> : IIndexController<T>, IIndex<T, TI>
    {
    }   

    public interface IUIndexG<T> : IIndexG<T>
    {
        T GetOfObj1(T x);
        bool RemoveOfObj1(T x);
    }
    public interface IUIndex<T, TI> : IUIndexG<T>, IIndex<T, TI>
    {
        T Get1(TI x);
        bool Remove1(TI x);
        void Index(TI x, Action<int> f);
        int Index(TI x); //because threadsafe
        int? Index_n(TI x);//because  threadsafe
    }

    public interface IUIndexControllerG<T> : IIndexController<T>, IUIndexG<T>
    {
        bool Test(T a, int ind);
        bool Test(T a);
    }
    public interface IUIndexController<T, TI> : IIndexController<T, TI>, IUIndexControllerG<T>, IUIndex<T, TI>
    {
    }
}
