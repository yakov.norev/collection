﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ZifLib
{
    public class KeyFoundException : Exception
    {
        public KeyFoundException()
        {
        }

        public KeyFoundException(string message) : base(message)
        {
        }

        public KeyFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected KeyFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
