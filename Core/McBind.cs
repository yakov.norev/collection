﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace ZifLib
{
    public class McBind<T> : MyCollection<T>, IBindingList
    {
        //public event ListChangedEventHandler();
        readonly IList<ListChangedEventHandler> listChangedEventHandlers = new List<ListChangedEventHandler>();
        public ISynchronizeInvoke synchronizeInvoke;

        public bool AllowNew => false;
        public bool AllowEdit => true;
        public bool AllowRemove => true;
        public bool SupportsChangeNotification => true;
        public bool SupportsSearching => true;
        public bool SupportsSorting => false;
        public bool IsSorted => false;

        public void OnListChanged(ListChangedEventArgs x)
        {
            if (synchronizeInvoke != null && synchronizeInvoke.InvokeRequired)
            {
                synchronizeInvoke.BeginInvoke((Action<ListChangedEventArgs>)X, new object[] { x });
            }
            else
                X(x);
        }

        private void X(ListChangedEventArgs x)
        {
            foreach (var item in listChangedEventHandlers)
            {
                item.DynamicInvoke(this, x);
            }
        }

        public event ListChangedEventHandler ListChanged
        {
            add => listChangedEventHandlers.Add(value);
            remove => listChangedEventHandlers.Remove(value);
        }

        public McBind(IList<T> items = null, bool Multithread = true) : base(items, Multithread)
        {
        }
        public override bool AddTry(T item)
        {
            var b = base.AddTry(item);
            if (b) OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, Count - 1));
            return b;
        }
        public override void RemoveAt(int ind)
        {
            base.RemoveAt(ind);
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, ind));
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, Count));
        }
        public override bool UpdateTry(int index, T value)
        {
            var r = base.UpdateTry(index, value);            
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index));            
            return r;
        }

        public void ResetItem(int index)
        {
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index));
        }
        #region NotImplementedException
        public PropertyDescriptor SortProperty => throw new NotImplementedException();
        public ListSortDirection SortDirection => throw new NotImplementedException();
        public object AddNew()
        {
            throw new NotImplementedException();
        }
        public void ApplySort(PropertyDescriptor property, ListSortDirection direction)
        {
            throw new NotImplementedException();
        }
        public int Find(PropertyDescriptor property, object key)
        {
            throw new NotImplementedException();
        }
        public void AddIndex(PropertyDescriptor property)
        {
            throw new NotImplementedException();
        }
        public void RemoveIndex(PropertyDescriptor property)
        {
            throw new NotImplementedException();
        }
        public void RemoveSort()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
