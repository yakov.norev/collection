﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZifLib
{
    public partial class MyCollection<T>
    {
        private static int iIndex;

        static string NewNameForIndex() => "Index_" + (iIndex++);// App.Rnd.Next().ToString();

        //IRelationTyped<T, T1> IndexColl<T>.CreateRel<T1>(Func<T, IDictionary<T1, object>> col)
        //{
        //	throw new NotImplementedException();
        //}

        IRelationTyped<T, T1> IndexColl<T>.CreateRel<T1>(Func<T, IDictionary<T1, object>> col)
        {
            var r = new Relation<T, T1>(this, col, NewNameForIndex());
            refs[r.Name] = r;
            for (int i = 0; i < items.Count; i++) r.AddIndex(i);
            return r;
        }
        IUIndex<T, T1> IndexColl<T>.CreateU<T1>(Func<T, T1> col, IEqualityComparer<T1> comparer, string name)
        {
            var n = new IndexU<T, T1>(this, name ?? NewNameForIndex(), col, comparer);
            ((IndexColl<T>)this).Create(n);
            return n;
        }
        IIndex<T, T1> IndexColl<T>.Create<T1>(Func<T, T1> col, bool unique, IEqualityComparer<T1> comparer, string name)
        =>
            ((IndexColl<T>)this).Create(unique ?
                (IIndexController<T, T1>)new IndexU<T, T1>(this, name ?? NewNameForIndex(), col, comparer)
                : new Index<T, T1>(this, name ?? NewNameForIndex(), col, comparer));


        IIndex<T, T1> IndexColl<T>.Create<T1>(IIndexController<T, T1> n)
      => locker.Write(() =>
      {
          indexes[n.Name] = n;
          for (int i = 0; i < items.Count; i++) n.AddIndex(i);
          return n;
      });

        public IndexColl<T> Indexs => this;
        IIndexG<T> IndexColl<T>.this[string key] => locker.Read(() => indexes[key]);
        IEnumerator<IIndexG<T>> IEnumerable<IIndexG<T>>.GetEnumerator() => locker.Read(() => indexes.Values.Cast<IIndexG<T>>().GetEnumerator());
    }
}
