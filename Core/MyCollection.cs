﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ZifLib.Utils.Collection;

namespace ZifLib
{
    /// <summary>
    /// 
    /// </summary>
    public class MyCollection
    {
        public static MyCollection<T> Create<T>(T prototype) => new MyCollection<T>();
        public static MyCollection<T> CreateList<T>(IList<T> a) => new MyCollection<T>(a);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public partial class MyCollection<T> : IList<T>, IList, ICollection, IndexColl<T>, IMCController<T>
    {
        private readonly IList<T> items;
        private readonly IDictionary<string, IIndexController<T>> indexes = new Dictionary<string, IIndexController<T>>();
        private readonly IDictionary<string, IRelationController<T>> refs = new Dictionary<string, IRelationController<T>>();
        private readonly IRWLocker locker = new RWLocker(TimeSpan.FromMinutes(0.5));

        public MyCollection(IList<T> items = null, bool Multithread = false)
        {
            //locker = Multithread ? (IRWLocker)new RWLockerOne() : new RWLockerFake();
            this.items = items ?? new List<T>();
        }
        public void Add(T item) => AssertKey(AddTry(item));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Insert(int index, T item) => AssertKey(InsertTry(index, item));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public void Update(int index, T item) => AssertKey(UpdateTry(index, item));

		/// <summary>
        /// 
        /// </summary>
        public void Clear() => locker.Write(() =>
        {
            indexes.Values.For(r => r.Clear());
            refs.Values.For(r => r.Clear());
            items.Clear();
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Remove(T item) => locker.Write(() =>
        {
            var ind = IndexOf(item);
            if (ind < 0) return false;
            RemoveAt(ind);
            return true;
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public virtual bool AddTry(T item) => locker.Write(() =>
        {
            if (Test(item)) return false;
            var index = items.Count;
            items.Add(item);
            UpdateIndex(index);
            return true;
        });

        /// <summary>
        /// вставка производится с переносом элемента по текущему индексу в конец
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool InsertTry(int index, T item) => locker.Write(() =>
        {
            if (Test(item)) return false;
            foreach (var il in indexes.Values) il.ReplaceIndex(index, Count);
            foreach (var il in refs.Values) il.ReplaceIndex(index, Count);
            items.InsertFast(index, item);
            UpdateIndex(index);
            return true;
        });

		
		void UpdateIndex(int c) => locker.Write(() =>
        {
            foreach (var inde in indexes.Values) inde.AddIndex(c);
            foreach (var inde in refs.Values) inde.AddIndex(c);
        });
        public void UpdateIndex(int c, Action<T> a) => locker.Write(() =>
        {
            a(this[c]);
            foreach (var inde in indexes.Values) inde.AddIndex(c);
            foreach (var inde in refs.Values) inde.AddIndex(c);
        });
		/// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual bool UpdateTry(int index, T value) => locker.Write(() =>
        {
            if (ReferenceEquals(items[index], value)) return true;
            if (Test(index, value)) return false;

            foreach (var inde in indexes.Values) inde.RemoveIndex(index);
            foreach (var inde in refs.Values) inde.RemoveIndex(index);
            items[index] = value;
            UpdateIndex(index);
            return true;
        });

        /// <summary>
        ///	удаление производится с переносом элемента с конца на текущий индекс		 
        /// </summary>
        /// <param name="index"></param>
        public virtual void RemoveAt(int index) => locker.Write(() =>
        {
            var indlast = items.Count - 1;
            if (index != indlast)
            {
                indexes.Values.For(il => il.RemoveReplaceIndex(index, indlast));
                refs.Values.For(r => r.RemoveReplaceIndex(index, indlast));
            }
            else
            {
                foreach (var inde in indexes.Values) inde.RemoveIndex(index);
                foreach (var inde in refs.Values) inde.RemoveIndex(index);
            }
            items.RemoveFast(index);
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index] { get => locker.Read(() => items[index]); set => Update(index, value); }
		
		
        IEnumerable<IUIndexControllerG<T>> UniqueIndexes => indexes.Values.OfType<IUIndexControllerG<T>>();
        /// <summary>
        /// проверка для замены существует ли объект сопадающий по уникальным индексам на отличающийся по местоположению от вставляемого
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>  
        /// <returns></returns>
        private bool Test(int index, T value) => UniqueIndexes.Any(ind => ind.Test(value, index));
        private bool Test(T item) => UniqueIndexes.Any(i => i.Test(item));
        private void AssertKey(bool b = false)
        {
			if (!b) throw new KeyFoundException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<T> GetEnumerator() => locker.Read(() => items.GetEnumerator());

        /// <summary>
        /// Определяет, содержит ли коллекция System.Collections.Generic.ICollection`1 указанное значение.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(T item) => locker.Read(() => items.Contains(item));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="arrayIndex"></param>
        public void CopyTo(T[] array, int arrayIndex) => locker.Read(() => items.CopyTo(array, arrayIndex));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int IndexOf(T item) => locker.Read(() => items.IndexOf(item));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="array"></param>
        /// <param name="index"></param>
        public void CopyTo(Array array, int index) => locker.Read(() => ((ICollection)items).CopyTo(array, index));
        /// <summary>
        /// 
        /// </summary>
        public int Count => locker.Read(() => items.Count);

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
        /// <summary>
        /// 
        /// </summary>

        public int Add(object value)
        {
            Add((T)value);
            return Count - 1;
        }
        public bool Contains(object value) => (value is T a) ? Contains(a) : false;
        public int IndexOf(object value) => (value is T a) ? IndexOf(a) : -1;
        public void Insert(int index, object value) => Insert(index, (T)value);
        public void Remove(object value) => Remove((T)value);

        public bool IsReadOnly => false;
        object ICollection.SyncRoot => locker;// :-)
        bool ICollection.IsSynchronized => true;
        IRWLocker IRWLockable.Locker => locker;

        public bool IsFixedSize => false;

        object IList.this[int index] { get => this[index]; set => this[index] = (T)value; }
    }
}
