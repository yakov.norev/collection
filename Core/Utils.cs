﻿using System;
using System.Collections.Generic;

namespace Core
{
	public static class Utils
	{
		public static T Next<T>(this Random rnd, IList<T> list) => list[rnd.Next(list.Count)];
		public static void RemoveFast<T>(this IList<T> li, int index)
		{
			li[index] = li[li.Count - 1];
			li.RemoveAt(li.Count - 1);
		}
		public static void InsertFast<T>(this IList<T> li, int index, T item)
		{
			li.Add(li[index]);
			li[index] = item;
		}
	}
}
