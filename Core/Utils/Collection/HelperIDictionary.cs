﻿using System;
using System.Collections.Generic;
namespace ZifLib.Utils.Collection
{
	public static class HelperIDictionary
	{
		private static readonly object nullObj = new object();
		public static TValue GetOrDefault<TValue, TKey>(this IDictionary<TKey, TValue> d, TKey key, TValue @default = default(TValue))
		{
			return d.TryGetValue(key, out var v) ? v : @default;
		}
		public static TValue? GetNullable<TValue, TKey>(this IDictionary<TKey, TValue> d, TKey key)
			where TValue : struct
		{
			return d.TryGetValue(key, out var v) ? (TValue?)v : null;            
		}

        /// <summary>
        /// ON - Null Object
        /// </summary>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="d"></param>
        /// <param name="key"></param>
        /// <param name="def"></param>
        /// <returns></returns>
		public static TValue GetON<TValue>(this IDictionary<object, TValue> d, object key, TValue def = default(TValue))
		{
			return d.GetOrDefault(key ?? nullObj, def);
		}
		public static TValue Create<TValue, TKey>(this IDictionary<TKey, TValue> d, TKey key)
				where TValue : new()
		{
			return d.Create(key, () => new TValue());
		}

		public static TValue Create<TValue, TKey>(this IDictionary<TKey, TValue> d, TKey key, Func<TValue> creator)
		{
			return d.TryGetValue(key, out var v) ? v : d[key] = creator();
		}
	}
}
