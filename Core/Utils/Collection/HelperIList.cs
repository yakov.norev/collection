﻿using System;
using System.Collections.Generic;
namespace ZifLib.Utils.Collection
{
    public static class HelperIList
    {
        public static T AddR<T>(this ICollection<T> coll, T x)
        {
            coll.Add(x);
            return x;
        }
        public static void AddRange<T>(this ICollection<T> coll, IEnumerable<T> x)
        {
            foreach (var item in x)
            {
                coll.Add(item);
            }
        }
        public static ICollection<T> CopyR<T>(this ICollection<T> a)
        {
            T[] x = new T[a.Count];
            a.CopyTo(x, 0);
            return x;
        }
        public static IList<T> CopyRW<T>(this ICollection<T> a)
        {
            List<T> x = new List<T>(a);
            return x;
        }
        public static T GetSafe<T>(this IList<T> t, int? i, T @default = default(T))
        {
            return i.HasValue && i >= 0 && i < t.Count ? t[i.Value] : @default;
        }

        public static int RemoveFast<T>(this IList<T> ps, Predicate<T> predicate)
        {
            var Num = 0;
            for (var index = 0; index < ps.Count;)
            {
                if (!predicate(ps[index])) { index++; continue; }
                Num++;
                ps.RemoveFast(index);
            }
            return Num;
        }
        public static IList<T> RemoveFastRet<T>(this IList<T> ps, Predicate<T> predicate)
        {
            var ret = new List<T>();
            for (var index = 0; index < ps.Count;)
            {
                if (!predicate(ps[index])) { index++; continue; }
                ret.Add(ps.RemoveFast(index));
            }
            return ret;
        }
        public static T RemoveFast<T>(this IList<T> li, int index)
        {
            var foo = li[index];
            if (index != li.Count - 1) li[index] = li[li.Count - 1];
            li.RemoveAt(li.Count - 1);
            return foo;
        }
        public static void InsertFast<T>(this IList<T> li, int index, T item)
        {
            if (li.Count == index)
            {
                li.Add(item);
            }
            else
            {
                li.Add(li[index]);
                li[index] = item;
            }
        }
        public static T Next<T>(this Random rnd, IList<T> list) => list[rnd.Next(list.Count)];
        public static T Rnd<T>(this IList<T> list, Random rnd = null) { rnd = rnd ?? new Random(); return list[rnd.Next(list.Count)]; }
    }
}
