﻿using System;
using System.Collections.Generic;
using System.Reflection;
namespace ZifLib.Utils.HelperOther
{
	public static class HelperOther1
	{
		public static T[] NewSize<T>(this T[] a, int size)
		{
			if (size == a.Length) return a;
			var _ = new T[size];
			Array.Copy(a, _, Math.Min(size, a.Length));
			return _;
		}
		public static T[] RawBuffer<T>(this List<T> l)
		{
			return l.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance)?.GetValue(l) as T[];
		}
        public static T Ё<T>(this T o,Action<T> a) {
            a(o);
            return o;
        }
	}
}
