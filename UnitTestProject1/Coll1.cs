﻿using System;
using ZifLib;

namespace UnitTestProject1
{
    public class StringIgnoreCase : IEquatable<StringIgnoreCase>, IEquatable<string>
    {
        string x;
        public StringIgnoreCase(string s)
        {
            x = s;
        }
        public string xLow => x.ToLower();

        public override bool Equals(object obj) =>
            obj is StringIgnoreCase sic ? Equals(sic) :
            obj is string str ? Equals(str) :
            false;

        public override int GetHashCode() => xLow.GetHashCode();
        public bool Equals(StringIgnoreCase other) => xLow.Equals(other.xLow);
        public bool Equals(string other) => xLow.Equals(other.ToLower());

        public static implicit operator StringIgnoreCase(string v) => new StringIgnoreCase(v);
    }
    public class Item
    {
        public StringIgnoreCase Id;
        public string Name;
        public Item(StringIgnoreCase id, string name)
        {
            Id = id;
            Name = name;
        }
    }
    public class Coll1 : MyCollection<Item>
    {
        public Coll1()
        {
            Id_Index = Indexs.Create(_ => _.Id);
            Name_Index = Indexs.Create(_ => _.Name);
            Id_Name_Index = Indexs.Create(_ => Tuple.Create(_.Id, _.Name), true);
        }

        public IIndex<Item, StringIgnoreCase> Id_Index { get; }
        public IIndex<Item, string> Name_Index { get; }
        public IIndex<Item, Tuple<StringIgnoreCase, string>> Id_Name_Index { get; }
    }
}
