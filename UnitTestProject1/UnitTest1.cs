﻿using System;
using System.Linq;
using System.Drawing;
using System.Threading;
using NUnit.Framework;
using System.Threading.Tasks;
using System.Collections.Generic;
using ZifLib;

namespace UnitTestProject1
{
	[TestFixture]
	public class UnitTest1
	{
		[Test]
		public void TestM1()
		{
			var c = MyCollection.Create(new { Id = 0, Name = default(string), Date = DateTime.Now, thr = default(Thread) });

			var id_ind = c.Indexs.Create(_ => _.Id);
			var name_ind = c.Indexs.Create(_ => _.Name);
			var id_name_ind = c.Indexs.Create(_ => new { _.Id, _.Name }, true);
			var _rnd = new Random();
			var NRem = 0;
			int Nthr = 100, Niter = 100;
			const int V = 100;
			var ts = Enumerable.Range(0, Nthr).Select(_ =>
			{
				var rnd = new Random(_rnd.Next());
				return new Task(() =>
				{
					for (var i = 0; i < Niter; i++)
					{						
						if (c.AddTry(new
						{
							Id = rnd.Next() % V,
							Name = rnd.Next() % V + "",
							Date = DateTime.Now,
							thr = Thread.CurrentThread
						})) Interlocked.Increment(ref NRem);
						var g = id_ind.Remove(rnd.Next() % V);
						for (int gi = 0; gi < g; gi++) Interlocked.Decrement(ref NRem);
					}
				});
			}).ToList();
			ts.ForEach(t => t.Start());
			Task.WhenAll(ts).Wait();

			Assert.AreEqual(NRem, c.Count);
		}

		[Test]
		public void TestMain()
		{
			var c = MyCollection.Create(new { id = default(int), name = default(string) });

			var id_index = c.Indexs.CreateU(_ => _.id);
			var name_ind = c.Indexs.Create(_ => _.name);
			c.Add(new { id = 5, name = "fdff" });
			c.Add(new { id = 0, name = "" });

			Assert.AreEqual("fdff", id_index.Get1(5).name);
			c[0] = new { id = 5, name = "kk" };

			Assert.AreEqual("kk", id_index.Get(5).First().name);

			Assert.False(c.UpdateTry(1, new { id = 5, name = "kkd" }));

			Assert.AreEqual("kk", id_index.Get(5).First().name);

			c.Insert(0, new { id = 7, name = "hhh" });
			Assert.AreEqual("hhh",c[0].name);
			Assert.AreEqual(new {id=5,name="kk" }, name_ind.Get("kk").FirstOrDefault());

			Assert.False(c.InsertTry(0, new { id = 7, name = "hhh" }));
		}

		[Test]
		public void TestMethod2()
		{
			var c = new Coll1();

			c.Add(new Item("4", "dfdg"));
			c.Add(new Item("4", "fff"));
			c.Add(new Item("4", "ggg"));
			c.Add(new Item("5", "dfdg"));
			c.Add(new Item("6", "dfdg"));
			var v = c.Id_Index.Get("4");
			Assert.AreEqual(3, v.Count);
			var v1 = c.Name_Index.Get("dfdg");
			Assert.AreEqual(3, v1.Count);
		}

		[Test]
		public void TestMethod4()
		{
			var c = new Coll1();

			c.Add(new Item("W", "dfdg"));
			c.Add(new Item("w", "fff"));

			Assert.Catch<KeyFoundException>(() => c.Add(new Item("W", "fff")));
			Assert.Catch<KeyFoundException>(() => c.Add(new Item("w", "fff")));

			c.Add(new Item("w", "ggg"));

			Assert.Catch<KeyFoundException>(() => c.Add(new Item("W", "ggg")));

			c.Add(new Item("w1", "dfdg"));
			c.Add(new Item("w2", "dfdg"));

			Assert.AreEqual(3, c.Id_Index.Get("w").Count);

			Assert.AreEqual(3, c.Name_Index.Get("dfdg").Count);
		}
        [Test]
        public void TestMethod5()
        {
            var collection1 = new Coll1();
            var rnd = new Random();
            var buf = new byte[2];

            var ProcessorCount = Environment.ProcessorCount;
            int countItem = 1000000 / ProcessorCount;
            int n = countItem * ProcessorCount;
            for (int i = 0; collection1.Count < n; i++)
            {
                rnd.NextBytes(buf);
                var a = Convert.ToBase64String(buf);
                rnd.NextBytes(buf);
                var b = Convert.ToBase64String(buf);
                try
                {
                    collection1.Add(new Item(a, b));
                }
                catch (KeyFoundException e)
                {
                }
            }

            //разбиваем коллекцию на несколько коллекций по количеству потоков           

            ////Рандомизируем массив

            var indli = Enumerable.Range(0, n).ToArray();
            Array.Sort(indli, (a, b) => rnd.Next(3) - 1);
            var NewColl = new Coll1();

            Task.WhenAll(
            Enumerable.Range(0, ProcessorCount)
             .Select(pi =>
            {
                var c1 = new Coll1();
                Enumerable.Range(0, countItem)
                .Select(i => collection1[indli[pi + i * ProcessorCount]])
                .ToList().ForEach(p => c1.Add(p));
                return c1;
            }).ToList()
             .Select(c1 => Task.Run(() => (c1 as IEnumerable<Item>).ToList().ForEach(item => NewColl.Add(item)))))
             .ContinueWith(a => Assert.AreEqual(NewColl.Count, collection1.Count)).Wait();
        }

        [Test]
		public void TestMethod7()
		{
			var c = MyCollection.Create(new { Id = 0, Name = "", Date = DateTime.Now });

			var id_ind = c.Indexs.Create(_ => _.Id);
			var name_ind = c.Indexs.Create(_ => _.Name);
			var id_name_ind = c.Indexs.Create(_ => new { _.Id, _.Name }, true);

			c.Add(new { Id = 0, Name = "", Date = DateTime.Now });
			c.Add(new { Id = 1, Name = "", Date = DateTime.Now });

			Assert.Catch<KeyFoundException>(() => c.Add(new { Id = 1, Name = "", Date = DateTime.Now }));
			Assert.Catch<KeyFoundException>(() => c.Add(new { Id = 1, Name = "", Date = DateTime.Now }));

			c.Add(new { Id = 0, Name = "sdf", Date = DateTime.Now });

			Assert.Catch<KeyFoundException>(() => c.Add(new { Id = 0, Name = "sdf", Date = DateTime.Now }));

			c.Add(new { Id = 1, Name = "sdf", Date = DateTime.Now });
			c.Add(new { Id = 0, Name = "fdsf", Date = DateTime.Now });

			Assert.AreEqual(3, id_ind.Get(0).Count);
			Assert.AreEqual(2, name_ind.Get("sdf").Count);
		}

		[Test]
		public void TestMethod8()
		{
			var c = MyCollection.Create(new { Id = 0, Name = default(string), Date = DateTime.Now });
			c.Clear();

			var id_ind = c.Indexs.Create(_ => _.Id);
			var name_ind = c.Indexs.Create(_ => _.Name);
			var id_name_ind = c.Indexs.Create(_ => new { _.Id, _.Name }, true);

			c.Clear();
			c.Clear();

			c.Add(new { Id = 0, Name = "", Date = DateTime.Now });
			c.Add(new { Id = 1, Name = "", Date = DateTime.Now });
			c.Clear();
			c.Add(new { Id = 0, Name = "", Date = DateTime.Now });
			c.Add(new { Id = 1, Name = "", Date = DateTime.Now });

			c.Add(new { Id = 0, Name = "sdf", Date = DateTime.Now });

			Assert.Catch<KeyFoundException>(() => c.Add(new { Id = 0, Name = "sdf", Date = DateTime.Now }));

			c.Add(new { Id = 1, Name = "sdf", Date = DateTime.Now });
			c.Add(new { Id = 0, Name = "fdsf", Date = DateTime.Now });

			Assert.AreEqual(3, id_ind.Get(0).Count);
			Assert.AreEqual(2, name_ind.Get("sdf").Count);
		}

		[Test]
		public void TestMethod1()
		{
			var c = MyCollection.Create(new { Id = 0, Name = default(string), Date = DateTime.Now });
			var id_ind = c.Indexs.Create(_ => _.Id);
			var name_ind = c.Indexs.Create(_ => _.Name);
			var id_name_ind = c.Indexs.Create(_ => new { _.Id, _.Name }, true);

			c.Add(new { Id = 5, Name = "sdfd", Date = DateTime.Now });
			c.Add(new { Id = 7, Name = "sdfd", Date = DateTime.Now });

			Assert.AreEqual("sdfd", id_ind.Get(5).First().Name);

			Assert.Zero(name_ind.RemoveOfObj(new { Id = 0, Name = "", Date = default(DateTime) }));
			Assert.Zero(name_ind.Remove(""));

			Assert.AreEqual(2, name_ind.Remove("sdfd"));
			Assert.Zero(name_ind.RemoveOfObj(new { Id = 0, Name = "sdfd", Date = default(DateTime) }));

			c.Add(new { Id = 5, Name = "sdfd", Date = DateTime.Now });
			c.Add(new { Id = 7, Name = "sdfd", Date = DateTime.Now });
			Assert.Catch(() => c.Add(new { Id = 5, Name = "sdfd", Date = DateTime.Now }));
			Assert.AreEqual(2, name_ind.RemoveOfObj(new { Id = 0, Name = "sdfd", Date = default(DateTime) }));
		}

		[Test]
		public void TestMethod1x()
		{
			var c = MyCollection.Create(new { Id = 0, Name = "", Date = DateTime.Now });
			var id_ind = c.Indexs.Create(_ => _.Id);
			var name_ind = c.Indexs.Create(_ => _.Name);
			var id_name_ind = c.Indexs.Create(_ => new { _.Id, _.Name }, true);

			c.Add(new { Id = 5, Name = "sdfd", Date = DateTime.Now });

			Assert.Zero(name_ind.RemoveOfObj(new { Id = 0, Name = "", Date = default(DateTime) }));
			Assert.Zero(name_ind.Remove(""));

			Assert.AreEqual(1, name_ind.Remove("sdfd"));


		}

		[Test]
		public void TestMethod3()
		{
			var c = MyCollection.Create(new
			{
				color = default(Color),
				mc = default(StringIgnoreCase),
				datetime = default(DateTime),
				thread = default(Thread)
			});

			c.Add(new { color = Color.Blue, mc = new StringIgnoreCase("MC"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Yellow, mc = new StringIgnoreCase("Mc"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Blue, mc = new StringIgnoreCase("mC"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Yellow, mc = new StringIgnoreCase("mc"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Blue, mc = new StringIgnoreCase("\\w"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Yellow, mc = new StringIgnoreCase("\\d"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Blue, mc = new StringIgnoreCase("\\d"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Yellow, mc = new StringIgnoreCase("\\w"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Blue, mc = new StringIgnoreCase("\\w"), datetime = DateTime.Now, thread = Thread.CurrentThread });
			c.Add(new { color = Color.Yellow, mc = new StringIgnoreCase("\\d"), datetime = DateTime.Now, thread = Thread.CurrentThread });

			var Ind_color = c.Indexs.Create(a => a.color, name: "color");
			var F = c.Indexs["color"].GetOfObj(new
			{
				color = Color.Blue,
				mc = default(StringIgnoreCase),
				datetime = default(DateTime),
				thread = Thread.CurrentThread
			});
			Assert.AreEqual(5, F.Count);

			var x = Ind_color.Get(Color.Yellow);
			Assert.AreEqual(5, x.Count);
			var Ind_reg = c.Indexs.Create(a => a.mc);
			var x1 = Ind_reg.Get("mc");
			Assert.AreEqual(4, x1.Count);
		}

		
	}
}
